1. To enable the NGINX Ingress controller, run the following command:
    minikube addons enable ingress

2. Verify that the NGINX Ingress controller is running:
    kubectl get pods -n ingress

3. Deploy Docker-image:
NOTE: If you use local image, you need add this command: minikube image load <name local image>
    kubectl create deployment web --image=<name local image>
    log: deployment.apps/web created

4. Expose the Deployment:
    kubectl expose deployment web --type=NodePort --port=8080
    log: service/web exposed

5. Verify the Service is created and is available on a node port:
    kubectl get service web
    log:
    NAME  |    TYPE     |  CLUSTER-IP     |  EXTERNAL-IP |  PORT(S)        |  AGE
    ------|-------------|-----------------|--------------|-----------------|------
    web   |    NodePort |  10.104.133.249 |  <none>      |  8080:31637/TCP |  12m

6. Visit the Service via NodePort:
    minikube service web --url
    log:
    The output is similar to:
        http://172.17.0.15:31637

Create an Ingress:

1. Create "example-ingress.yaml" from the following file:
    https://k8s.io/examples/service/networking/example-ingress.yaml

2. Create the Ingress object by running the following command:
    kubectl apply -f https://k8s.io/examples/service/networking/example-ingress.yaml
    log:
    ingress.networking.k8s.io/example-ingress created

3. Verify the IP address is set:
    kubectl get ingress
    log:
    NAME            |  CLASS   | HOSTS            |  ADDRESS      |  PORTS  | AGE
    ----------------|----------|------------------|---------------|---------|----
    example-ingress |  <none>  | hello-world.info |  172.17.0.15  |  80     | 38s

4. Add the following line to the bottom of the /etc/hosts file on your computer (you will need administrator access):
    172.17.0.15 hello-world.info
    NOTE:
        If you are running Minikube locally, use minikube ip to get the external IP. The IP address displayed within the ingress list will be the internal IP. 
    After you make this change, your web browser sends requests for hello-world.info URLs to Minikube.